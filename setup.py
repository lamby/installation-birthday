#!/usr/bin/env python3

from setuptools import setup


setup(
    name="installation-birthday",
    author="Chris Lamb",
    author_email="lamby@debian.org",
    scripts=("installation-birthday",),
)
